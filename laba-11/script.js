Vue.component('mytitle', {
    props: ['titleurl'],
    template: '<h1><a v-bind:href="#">Форма посещаемости студентов</a></h1>'
})
Vue.component('mycomponent', {
    data: function ()
    {
        return {
            students: [],
            name: "",
            surname: ""
        };
    },
    methods: {
        onClick: function (msg, event)
        {
            if (event) {
                event.preventDefault()
            }
            this.students.push(this.fullName);
        }
    },
    computed: {
        
        fullName: function ()
        {
                    const today = new Date();
                    const date = today.getDate()+'.'+(today.getMonth()+1)+'.'+today.getFullYear();
                    const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
                    const dateTime = 'Дата: ' + date +' Время: '+ time;
		   
		   
            return "Имя: " + this.name + " Фамилия: " + this.surname + " " + dateTime
        }
    },
    template: `
    <div>
    <form id="myform">
    <div class="form-group">
      <label for="name">Имя</label>
      <input type="text" id="name" name="name" required class="form-control" placeholder="Имя..." v-model="name">
    </div>
    <div class="form-group">
      <label for="surname">Фамилия</label>
      <input type="text" id="surname" required name="surname" class="form-control" placeholder="Фамилия..." v-model="surname">
    </div>
    <button type="submit" class="btn btn-primary" v-on:click.prevent="onClick">Записать</button>
  </form>
  <template v-if="students.length">
  <ul>
    <li v-for="student in students" :key="student">{{student}}</li>
  </ul>
  </template>
  <template v-else>
    <p>Пока нет записей. Добавьте новую.</p>
  </template>
  </div>
  `
})

var app = new Vue({
    el: '#vueapp',
    data: {
        titleurl: "/"
    },

})
//Орлов В.А.