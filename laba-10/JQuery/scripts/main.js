$(document).ready(function(){
    $("p").click(function(){
        $(this).hide();
    });
    let isHide = false;
    $("#button").click(function(){
        if(isHide == false){
            $("#test").hide();
            isHide = true;
            $("#button").text("Вернуть блок ниже");
        }
        else if(isHide){
            $("#test").show();
            isHide = false;
            $("#button").text("Убрать блок ниже");
        }
        
    });
    $("#onMe").mouseenter(function(){
        alert("Осторожно! Закройте это окно.");
    });
    $("#onemore").on({
        mouseenter: function(){
            $(this).css("background-color", "lightgray");
        },
        mouseleave: function(){
            $(this).css("background-color", "lightblue");
        },
        click: function(){
            $(this).css("background-color", "yellow");
        }
    });
    $("#button2").click(function(){
        $("#div1").fadeToggle();
        $("#div2").fadeToggle("slow");
        $("#div3").fadeToggle(3000);
    });
});